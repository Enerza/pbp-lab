from django import forms
from django.forms import widgets
from lab_2.models import Note


class TextInput(forms.Textarea):
    input_type = 'text'

class NoteForm(forms.ModelForm):
    
    class Meta:
        model = Note
        fields = '__all__'
        widgets = {
            'To': forms.TextInput(attrs={'spellcheck': 'false'}),
            'From': forms.TextInput(attrs={'spellcheck': 'false'}),
            'Title': forms.TextInput(attrs={'spellcheck': 'false'}),
            'Message': forms.Textarea(attrs={'spellcheck': 'false'}),
        }