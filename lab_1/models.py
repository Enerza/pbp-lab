from django.db import models
from datetime import date

class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.CharField(max_length=20)
    dob = models.DateField(default=date.today)

    def __str__(self):
        return self.name
