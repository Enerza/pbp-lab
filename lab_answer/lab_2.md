1. Apakah perbedaan antara JSON dan XML?
JSON
- data format
- tidak bisa dicommment
- support array
XML
- markup language, terdapat tag-tag
- bisa dicomment
- tidak support array

https://www.geeksforgeeks.org/difference-between-json-and-xml/
https://www.imaginarycloud.com/blog/json-vs-xml/

2. Apakah perbedaan antara HTML dan XML?
HTML digunakan untuk menampilkan data dan struktur webpage. -> statis, tag untuk menampilkan data
XML digunakan untuk menyimpan dan transfer data. -> dinamis, tag untuk deskripsi data


https://www.upgrad.com/blog/html-vs-xml/